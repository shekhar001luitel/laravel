<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\SettingController;


Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/register', [RegisterController::class, 'show'])->name('register.show');
    Route::post('/register', [RegisterController::class, 'register'])->name('register.perform');
    Route::get('/login', [LoginController::class, 'show'])->name('login.show');
    Route::post('/login', [LoginController::class, 'login'])->name('login.perform');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [SettingController::class, 'index'])->name('dashboard');
    Route::get('/delete/{id}', [SettingController::class, 'deleteUser'])->name('delete');
    Route::get('/setting', [SettingController::class, 'index'])->name('setting');
    Route::get('/logout', [LogoutController::class, 'perform'])->name('logout.perform');
});
