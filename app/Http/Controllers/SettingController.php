<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $userData = User::all();

        $userDataPagination = User::paginate(2);
        $id = Auth::id();
        dd(User::all()->where('id', $id));
        return view('home/dashboard', compact('userData', 'userDataPagination'));
    }
    public function deleteUser($id)
    {
        User::find($id)->delete();
    }
}
